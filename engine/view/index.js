"use strict";
const express = require('express');
const exphbs = require('express-hbs');
const path = require('path');

const ViewEngine = function ViewEngine(engine) {
  const self = this;
  const app = engine.app;

  const paths = {
    views: [
      path.join(__dirname, '..', '..', 'views')
    ],
    partials: [
      path.join(__dirname, '..', '..', 'views', 'partials')
    ],
    layouts: path.join(__dirname, '..', '..', 'views', 'layouts')
  };

  self.init = () => {
    app.locals.engine = engine;
    app.locals.version = engine.version.version;

    app.set('views', paths.views);
    app.engine('.hbs', exphbs.express4({
      defaultLayout: `${__dirname}/../../views/main`,
      partialsDir: paths.partials,
      layoutsDir: paths.layouts
    }));
    app.set('view engine', '.hbs');
    app.use('/', express.static(path.join(__dirname, '..', '..', 'public')));
  };

  self.handlebars = exphbs.handlebars;

  self.registerView = function registerView(root, routes) {
    if (!routes) {
      routes = 'views';
    }

    paths.views.push(path.join(root, routes));
  };

  self.registerPartial = function registerView(root, routes) {
    if (!routes) {
      routes = 'partials';
    }

    paths.partials.push(path.join(root, routes));
  };

  return self;
};

module.exports = function View(engine) {
  return new ViewEngine(engine);
};
