"use strict";
const RouteAccessException = require("./RouteAccessException");
const _ = require('lodash');

class Socket {
  constructor(engine, socket, broadcast) {
    this.engine = engine;
    this.app = engine.app;

    this.socket = socket;
    this.broadcast = broadcast;
    this.path = "";

    this.init();
    this.build();
  }

  init() {

  }

  build() {

  }

  register(action, fn) {
    this.socket.on(this.listen(action), (data) => {
      if (!this.allowed(action, `${this.path}.${action}`)) {
        return null;
      }

      return fn(this, this.socket.request, data);
    });
  }

  registerAll(action) {
    this.socket.on(this.listen(action), (data) => this[action](this, data));
  }

  listen(action) {
    return `${this.path}:${action}`;
  }

  emit(action) {
    return `${this.path}.return:${action}`;
  }

  isAuthenticated(user) {
    return _.isObject(user);
  }

  allowed(page, permission) {
    const user = this.socket.request.user;

    if (!this.isAuthenticated(user)) {
      throw new RouteAccessException(page);
    }

    if (user.isAdmin()) {
      return true;
    }

    return user.acl.allowed(permission);
  }
}

module.exports = Socket;
