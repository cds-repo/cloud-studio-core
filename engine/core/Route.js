"use strict";
const express = require("express");
const RouteAccessException = require("./RouteAccessException");
const _ = require('lodash');

class Route {
  constructor(engine, path) {
    this.engine = engine;
    this.app = engine.app;

    this.router = express.Router();
    this.path = path;
  }

  register() {
    this.router.get('/', (req, res) => {
      res.json({
        hello: 'world'
      });
    });
  }

  build() {
    this.register();
    this.app.use(`/${this.path}`, this.router);
  }

  isAuthenticated(user) {
    return _.isObject(user);
  }

  redirectUnauthenticated(user, res) {
    if (!this.isAuthenticated(user)) {
      res.redirect('/');
    }
  }

  allowed(user, page, permission) {
    if (!this.isAuthenticated(user)) {
      throw new RouteAccessException(page);
    }

    if (user.isAdmin()) {
      return true;
    }

    return user.acl.allowed(permission);
  }
}

module.exports = Route;
