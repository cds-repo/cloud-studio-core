"use strict";

class RouteAccessException extends Error {
  constructor(page) {
    super(`You cannot access this section [${page}]!`);
  }
}

module.exports = RouteAccessException;
