"use strict";

class APIAccessException extends Error {
  constructor(page) {
    super(`You cannot access this section [${page}]!`);
  }
}

module.exports = APIAccessException;
