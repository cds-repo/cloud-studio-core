"use strict";
const _ = require("lodash");
let self;

class Modules {
  constructor(engine) {
    const packageJson = require('../../package.json');
    this.CDSModules = packageJson.CDSModules;
    this.modules = {};
    this.engine = engine;
    self = this;
  }

  load() {
    _.forOwn(this.CDSModules, (module) => {
      self.engine.log.debug('Loading module: [%s].', module);
      self.modules[module] = require(_.join(['../../modules', module], '/'))(self.engine);
      self.engine.log.debug('Module [%s] was loaded successfully.', module);
    });
  }

  get(module) {
    return this.modules[module];
  }
}

module.exports = Modules;
