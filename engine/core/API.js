"use strict";
const express = require("express");
const APIAccessException = require("./APIAccessException");
const _ = require('lodash');

class API {
  constructor(engine, path) {
    this.engine = engine;
    this.app = engine.app;

    this.router = express.Router();
    this.path = path;
  }

  register() {
    this.router.get('/', (req, res) => {
      res.json({
        hello: 'world'
      });
    });
  }

  build() {
    this.register();
    this.app.use(`/api/v1/${this.path}`, this.router);
  }

  allowed(user, page, permission) {
    if (!_.isObject(user)) {
      throw new APIAccessException(page);
    }

    if (user.isAdmin()) {
      return true;
    }

    return user.acl.allowed(permission);
  }
}

module.exports = API;
