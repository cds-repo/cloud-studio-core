"use strict";
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const Log = require('log');
const socketIo = require('socket.io');
const http = require('http');
const _ = require('lodash');

let View = require('./view');
let Permission = require('./core/permission');

const Engine = {
  version: require('./base/version'),
  config: require('./config'),
  core: require('./core'),
  ACL: require('./acl'),
  permissions: new Permission(),
  app: express()
};

/* TODO: Extend config with .env/.rc files */
const config = Engine.config;
const app = Engine.app;
Permission = Engine.permissions;
View = Engine.views = View(Engine);

const server = http.Server(app);
Engine.log = new Log(config.engine.debug ? "debug" : config.engine.log);
Engine.io = socketIo(server);

const io = Engine.io;

Engine.routes = Engine.core.routes(Engine);

app.use(cors({
  credentials: true,
  origin: config.http.cors
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());

app.use((req, res, next) => {
  res.ok = function ok(message) {
    return res.json({
      status: "ok",
      message
    });
  };

  res.error = function error(type, message, code) {
    return res.status(code || 500).json({
      status: "error",
      type,
      message
    });
  };

  res.throw = function throws(error) {
    const errorType = error.constructor.name;
    let errorMessage = error.message;
    const code = error.httpCode || 500;

    if (_.isEmpty(errorType) || errorType === "Error") {
      // Engine.log.error(errorMessage);
      Engine.log.error(error.stack);
      errorMessage = "An error occured, please consult the log!";
    }

    return res.error(errorType, errorMessage, code);
  };

  next();
});

Engine.modules = new Engine.core.Modules(Engine);
Engine.modules.load();
View.init();

switch (config.session.store) {
  case 'memory':
    app.use(session({
      cookieParser: cookieParser(),
      key: config.session.key,
      secret: config.session.secret,
      resave: false,
      saveUninitialized: true
    }));
    break;
}

module.exports = function start() {
  const log = Engine.log;

  server.listen(config.http.port);

  io.sockets.on('connection', (socket) => {
    log.debug("New Connection");

    _.each(Engine.modules.modules, (v, k) => {
      if (!_.isObject(v) || _.isNull(v.sockets)) {
        return;
      }

      if (_.isNull(v.sockets) || _.isUndefined(v.sockets)) {
        return;
      }

      log.debug("Registered element for connection");
      v.sockets(Engine, socket, io.sockets);
    });

    socket.on("disconnect", () => {
      log.debug("Disconnected");
    });
  });

  log.info('%s [v%s]', config.engine.name || "Cloud Studio Framework", Engine.version.version);
  log.info('-'.repeat(40));
  log.info('Host: %s', config.http.host);
  log.info('Port: %d', config.http.port);
  log.info('-'.repeat(40));

  app.use('*', (req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  app.use((err, req, res, next) => {
    log.error(req.path);
    log.error(err.stack);

    let statusCode = err.status || 500;
    switch (err.name) {
      case 'ValidationError':
        statusCode = 400;
        break;
    }

    res.status(statusCode || 500).json({
      status: "error",
      type: err.name,
      message: err.message
    });
  });

  log.info('Everything is ok...');
};
