"use strict";
const consign = require('consign');
const _ = require('lodash');
let config = {};

config.init = function init() {
  consign({
    cwd: 'config',
    verbose: false,
    extensions: ['.js', '.json', '.node'],
    loggingType: 'info'
  })
    .include('../config')
    .into(config);
};

config.reset = function reset() {
  config.init();
};

config.load = function load(file) {
  const val = require(file);

  config = _.extend(config, val);
};

config.init();

module.exports = config;
