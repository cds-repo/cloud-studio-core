"use strict";
const _ = require('lodash');

const Version = {
  major: 0,
  minor: 2,
  patch: 0,

  codename: 'Dawn'
};

module.exports = {
  get version() {
    return _.join([Version.major, Version.minor, Version.patch], '.');
  },

  get codename() {
    return _.join([this.version, Version.codename], '-');
  }
};
