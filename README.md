# Cloud Studio Core

This represents the Core part of the Cloud Studio Suite editor. Based on this project various versions will be created.

## Contributing

You can contribute to this project by adding issues, fixing them and making pull requests.