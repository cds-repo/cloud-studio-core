/* global describe, it, before, beforeEach, after, afterEach */
"use strict";
const assert = require('assert');
const _ = require('lodash');

const config = require('../../../engine/config');

describe('Engine.Config unit test', () => {
  it('should have elements', (done) => {
    assert.notEqual(Object.keys(config).length, 0);
    done();
  });

  it('should contain [http] element', (done) => {
    assert.ok(_.isObject(config.http));
    done();
  });
  it('should contain [engine] element', (done) => {
    assert.ok(_.isObject(config.engine));
    done();
  });
  it('should contain [socket] element', (done) => {
    assert.ok(_.isObject(config.socket));
    done();
  });

  it('should read js config', (done) => {
    config.reset();
    assert.notEqual(config.http.port, 7000);
    config.load('../../tests/mockup/test.config.js');
    assert.equal(config.http.port, 7000);
    done();
  });

  it('should read json config', (done) => {
    config.reset();
    assert.notEqual(config.http.port, 7500);
    config.load('../../tests/mockup/test.config.json');
    assert.equal(config.http.port, 7500);
    done();
  });
});
