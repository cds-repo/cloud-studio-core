"use strict";

module.exports = function CLIParams(engine) {
  const appArgs = process.argv;

  if (appArgs.length > 2) {
    engine.config.fs.path = appArgs[2];
  }

  engine.log.info("Workspace Location:", engine.config.fs.path);
};
