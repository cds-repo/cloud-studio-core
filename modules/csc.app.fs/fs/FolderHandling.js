"use strict";

const FSBase = require("./FSBase");
const Exceptions = require("../exceptions");
const _ = require("lodash");
const FileType = require("./FileType");
const dateFormat = require("dateformat");
const fstream = require('fstream');
const tar = require('tar');
const zlib = require('zlib');

class FolderHandling extends FSBase {
  constructor(engine) {
    super(engine);

    this._id = 0;
  }

  get BYTESIZE() {
    return 1024;
  }

  get KILOSIZE() {
    return this.BYTESIZE * 1024;
  }

  get MEGASIZE() {
    return this.KILOSIZE * 1024;
  }

  get GIGASIZE() {
    return this.MEGASIZE * 1024;
  }

  _computeSize(size) {
    if (size < this.BYTESIZE) {
      return `${size} B`;
    }

    if (size >= this.BYTESIZE && size < this.KILOSIZE) {
      return `${(parseFloat(size / this.BYTESIZE).toFixed(2))} KB`;
    }

    if (size >= this.KILOSIZE && size < this.MEGASIZE) {
      return `${(parseFloat(size / this.KILOSIZE).toFixed(2))} MB`;
    }

    if (size >= this.MEGASIZE && size < this.GIGASIZE) {
      return `${(parseFloat(size / this.MEGASIZE).toFixed(2))} GB`;
    }

    return "";
  }

  _fileLister(filename) {
    const stats = this.fs.lstatSync(filename);

    let content = [];

    const info = {
      id: this._id++,
      file: this.path.basename(filename)
    };

    if (stats.isDirectory()) {
      content = this.fs
        .readdirSync(filename)
        .filter((element) => this._filterHidden(element))
        .map((child) => this._fileLister(`${filename}/${child}`));

      info.type = "directory";
      info.content = content;
    } else {
      // Assuming it's a file. In real life it could be a symlink or
      // something else!
      info.type = "file";
    }

    info.fileType = info.type === "file" ? FileType(filename) : '';
    info.size = info.type === "directory" ? '' : this._computeSize(stats.size);

    if (info.fileType === "image") {
      info.type = "image";
    }

    return info;
  }

  _deleteFolderRecursive(path) {
    if (this.fs.existsSync(path)) {
      this.fs.readdirSync(path).forEach((file) => {
        const curPath = `${path}/${file}`;
        if (this.fs.lstatSync(curPath).isDirectory()) {
          this._deleteFolderRecursive(curPath);
        } else {
          this.fs.unlinkSync(curPath);
        }
      });
      this.fs.rmdirSync(path);
    }
  }

  _filterHidden(element) {
    const toIgnore = [
      ".DS_Store",
      ".cdsfs",
      ".git",
      ".hg"
    ];
    return toIgnore.indexOf(element) < 0;
  }

  create(folder) {
    if (!folder) {
      throw new Exceptions.NoFileException();
    }
    return this.fs.mkdirp.mkdirpAsync(this.fullPath(folder));
  }

  delete(folder) {
    if (!folder) {
      throw new Exceptions.NoFileException();
    }

    return this._deleteFolderRecursive(this.fullPath(folder));
  }

  readTree(folder) {
    try {
      this._id = 0;
      return this._fileLister(this.fullPath(folder)).content;
    } catch (e) {
      return [];
    }
  }

  read(folder) {
    let id = 0;
    const fullPath = this.fullPath(folder);

    const data = this.fs.readdirSync(fullPath)
      .filter((element) => this._filterHidden(element))
      .map((element) => {
        const stat = this.fs.lstatSync(`${fullPath}/${element}`);

        const newElement = {
          id: ++id,
          type: stat.isDirectory() ? "directory" : "file",
          name: element,
          modify: dateFormat(stat.mtime, "HH:MM:ss @ dd mm yyyy")
        };

        newElement.fileType = newElement.type === "file" ? FileType(element) : '';
        newElement.size = newElement.type === "directory" ? '' : this._computeSize(stat.size);

        if (newElement.fileType === "image") {
          newElement.type = "image";
        }

        return newElement;
      });

    return _.sortBy(data, (element) => element.type);
  }

  copy(sourceFile, destFile, options) {
    if (!sourceFile) {
      throw new Exceptions.NoSourceException();
    }
    if (!destFile) {
      throw new Exceptions.NoDestinationException();
    }

    options = _.extend({
      filter: (file) => this._filterHidden(file),
      overwrite: true,
      dot: true
    }, options);

    return this.fs.recursiveCopy(this.fullPath(sourceFile), this.fullPath(destFile), options);
  }

  pack(folder, destination) {
    if (!folder) {
      throw new Exceptions.NoFileException();
    }

    if (!destination) {
      throw new Exceptions.NoDestinationException();
    }

    const self = this;

    return new Promise((resolve, reject) => {
      let filename = destination;
      if (!_.endsWith(filename, '.tar.gz')) {
        filename = `${destination}.tar.gz`;
      }

      const writer = fstream
        .Writer({
          path: self.fullPath(filename)
        });

      fstream
        .Reader({
          path: self.fullPath(folder),
          type: 'Directory'
        })
        .pipe(tar.Pack())/* Convert the directory to a .tar file */
        .pipe(zlib.Gzip())/* Compress the .tar file */
        .pipe(writer);

      writer.on("close", resolve);
      writer.on("error", reject);
    });
  }

}

module.exports = FolderHandling;
