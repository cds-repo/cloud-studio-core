"use strict";

const FSBase = require("./FSBase");
const mime = require("mime");
const Exceptions = require("../exceptions");
const _ = require("lodash");

class FileHandling extends FSBase {
  create(file) {
    if (!file) {
      throw new Exceptions.NoFileException();
    }

    return this.save(file, '');
  }

  delete(file) {
    if (!file) {
      throw new Exceptions.NoFileException();
    }

    return this.fs.unlinkSync(this.fullPath(file));
  }

  read(file) {
    if (!file) {
      throw new Exceptions.NoFileException();
    }

    if (mime.lookup(this.fullPath(file)).startsWith('image/')) {
      return this.fs.readFileAsync(this.fullPath(file));
    }

    return this.fs.readFileAsync(this.fullPath(file), 'utf8');
  }

  save(file, contents) {
    if (!file) {
      throw new Exceptions.NoFileException();
    }

    return this.fs.writeFileAsync(this.fullPath(file), contents, 'utf8');
  }

  clear(file) {
    return self.create(file);
  }

  size(file) {
    if (!file) {
      throw new Exceptions.NoFileException();
    }

    return this.fs.statSync(this.fullPath(file)).size;
  }

  truncate(file, length) {
    if (!file) {
      throw new Exceptions.NoFileException();
    }

    if (!_.isNumber(length)) {
      length = 0;
    }

    if (!this.exists(file)) {
      throw new Exceptions.FileNotExistsException(file);
    }

    return this.fs.truncateAsync(this.fullPath(file), length);
  }

  copy(sourceFile, destFile, options) {
    if (!sourceFile) {
      throw new Exceptions.NoSourceException();
    }
    if (!destFile) {
      throw new Exceptions.NoDestinationException();
    }

    if (!options) {
      options = {
        clobber: false
      };
    }

    return this.fs.copyAsync(this.fullPath(sourceFile), this.fullPath(destFile), options);
  }

}

module.exports = FileHandling;
