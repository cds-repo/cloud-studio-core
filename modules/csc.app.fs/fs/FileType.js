"use strict";

module.exports = (name) => {
  let type = "text";
  let extSplit = name.split('.');

  extSplit = extSplit[extSplit.length - 1].toLowerCase();

  switch (extSplit) {
    case "js":
      type = "javascript";
      break;

    case "html":
    case "xml":
    case "svg":
      type = "markup";
      break;

    case "json":
    case "css":
    case "less":
    case "sass":
    case "scss":
    case "php":
      type = extSplit;
      break;

    case "sh":
      type = "bash";
      break;

    case 'jpg':
    case 'png':
    case 'gif':
    case 'ico':
      type = "image";
      break;
  }

  return type;
};
