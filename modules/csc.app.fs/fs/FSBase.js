﻿"use strict";
const Promise = require("bluebird");
const path = require("path");
const fs = Promise.promisifyAll(require("fs"));
const mkdirp = Promise.promisifyAll(require("mkdirp"));
const recursiveCopy = require("recursive-copy");
const _ = require("lodash");
const Exceptions = require("../exceptions");
const PathInfo = require("pathinfo");

class FSBase {
  constructor(engine) {
    this.engine = engine;
    this.fs = fs;
    this.fs.mkdirp = mkdirp;
    this.fs.recursiveCopy = recursiveCopy;
    this.path = path;
  }

  get EXISTS() {
    return fs.F_OK;
  }

  get WRITE() {
    return fs.W_OK;
  }

  get READ() {
    return fs.R_OK;
  }

  get EXECUTE() {
    return fs.X_OK;
  }

  pathinfo(localPath) {
    return PathInfo(this.fullPath(localPath));
  }

  fullPath(localPath) {
    return path.resolve(`${this.engine.config.fs.path}/${localPath}`);
  }

  exists(localPath) {
    if (!localPath) {
      throw new Exceptions.NoFileException();
    }

    return fs.existsSync(this.fullPath(localPath));
  }

  access(localPath, mode) {
    if (!localPath) {
      throw new Exceptions.NoFileException();
    }

    if (!this.exists(localPath)) {
      throw new Exceptions.FileNotExistsException(localPath);
    }

    if (!_.isNumber(mode)) {
      mode = 0;
    }

    return fs.accessAsync(this.fullPath(localPath), mode);
  }

  stats(localPath) {
    if (!localPath) {
      throw new Exceptions.NoFileException();
    }

    if (!this.exists(localPath)) {
      throw new Exceptions.FileNotExistsException(localPath);
    }

    return fs.statAsync(this.fullPath(localPath));
  }

  chmod(localPath, mode) {
    if (!localPath) {
      throw new Exceptions.NoFileException();
    }

    if (mode === undefined || mode === null) {
      throw new Exceptions.NoModeException();
    }

    if (!this.exists(localPath)) {
      throw new Exceptions.FileNotExistsException(localPath);
    }

    return fs.chmodAsync(this.fullPath(localPath), mode);
  }

  rename(localPath, oldFileName, newFileName) {
    if (!localPath) {
      throw new Exceptions.NoFileException();
    }

    if (!oldFileName) {
      throw new Exceptions.NoSourceException();
    }
    if (!newFileName) {
      throw new Exceptions.NoDestinationException();
    }

    return this.move(`${localPath}/${oldFileName}`, `${localPath}/${newFileName}`);
  }

  move(oldFile, newFile) {
    if (!oldFile) {
      throw new Exceptions.NoSourceException();
    }
    if (!newFile) {
      throw new Exceptions.NoDestinationException();
    }

    return fs.renameAsync(this.fullPath(oldFile), this.fullPath(newFile));
  }
}

module.exports = FSBase;
