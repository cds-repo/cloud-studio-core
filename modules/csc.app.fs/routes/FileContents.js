"use strict";
const API = require("../../../engine/core/API");
const FileHandling = require("../fs/FileHandling");
const FolderHandling = require("../fs/FolderHandling");
const FileType = require("../fs/FileType");
const _ = require("lodash");

class FileContents extends API {
  constructor(engine) {
    super(engine, "workspace");
    this.fs = {
      FileHandling: new FileHandling(engine),
      FolderHandling: new FolderHandling(engine)
    };
  }

  register() {
    this.router.get('/tree', (req, res) => this.getFileTree(this, req, res));
    this.router.get('/listing', (req, res) => this.getListing(this, req, res));
    this.router.get('/read', (req, res) => this.getFile(this, req, res));
    this.router.put('/save', (req, res) => this.putSaveFile(this, req, res));
    this.router.post('/create', (req, res) => this.postCreateFile(this, req, res));
    this.router.delete('/delete', (req, res) => this.deleteFile(this, req, res));
    this.router.put('/move', (req, res) => this.putMove(this, req, res));
  }


  getFileTree(self, req, res) {
    res.json(self.fs.FolderHandling.readTree(req.query.path));
  }

  getListing(self, req, res) {
    res.json(self.fs.FolderHandling.read(req.query.path));
  }

  getFile(self, req, res) {
    self.fs.FileHandling
      .read(req.query.path)
      .then((result) => {
        res.json(self._readFile(result, req.query.path));
      })
      .catch((e) => {
        res.throw(e);
      });
  }

  _readFile(result, path) {
    let type = "file";
    if (typeof(result) === "object") {
      type = "image";
      result = result.toString("base64");
    }

    const fileInfo = this.fs.FileHandling.pathinfo(path);
    let lang = fileInfo.extname || ".txt";
    if (lang.length !== 0) {
      lang = lang.substring(1);
    }

    let pathDetails = path;
    pathDetails = pathDetails.substring(0, pathDetails.length - fileInfo.filename.length);


    return {
      name: fileInfo.filename,
      path: pathDetails,
      content: result,
      lang: FileType(lang),
      type
    };
  }

  putSaveFile(self, req, res) {
    self.fs.FileHandling
      .save(req.body.path, req.body.content)
      .then(() => res.ok("File saved"))
      .catch((e) => {
        res.throw(e);
      });
  }

  postCreateFile(self, req, res) {
    let q;
    if (req.body.type && req.body.type === "folder") {
      q = self.fs.FolderHandling.create(req.body.path)
        .then(() => res.ok("Folder created"));
    } else {
      q = self.fs.FileHandling.create(req.body.path)
        .then(() => self.fs.FileHandling
          .read(req.body.path))
        .then((result) => {
          const data = self._readFile(result, req.body.path);

          res.json(_.extend(data, {
            message: "File saved",
            status: "ok"
          }));
        });
    }

    q.catch((e) => {
      res.throw(e);
    });
  }

  deleteFile(self, req, res) {
    if (req.body.type && (req.body.type === "directory" || req.body.type === "folder")) {
      self.fs.FolderHandling.delete(req.body.path);
      res.ok("Folder deleted");
    } else {
      self.fs.FileHandling.delete(req.body.path);
      res.ok("File deleted");
    }
  }

  putMove(self, req, res) {
    const source = req.body.source;
    const dest = req.body.destination;

    return self.fs.FileHandling
      .move(source, dest)
      .then(() => res.ok("Item moved"))
      .catch((e) => {
        res.throw(e);
      });
  }
}

module.exports = FileContents;
