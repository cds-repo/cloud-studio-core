"use strict";

class NoFileException extends Error {
  constructor() {
    super("You haven't specified any file or folder!");
  }
}

module.exports = NoFileException;
