"use strict";

const FileHandling = require("./fs/FileHandling");
const FolderHandling = require("./fs/FolderHandling");

module.exports = function FS(engine) {
  const self = {};
  self.FileHandling = new FileHandling(engine);
  self.FolderHandling = new FolderHandling(engine);

  engine.routes.build(__dirname);

  return self;
};
