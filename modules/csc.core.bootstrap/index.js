"use strict";

module.exports = function Bootstrap(engine) {
  engine.routes.build(__dirname);
  engine.views.registerView(__dirname);
};
