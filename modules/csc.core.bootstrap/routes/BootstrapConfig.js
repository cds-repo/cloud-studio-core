"use strict";
const API = require("../../../engine/core/API");
const _ = require("lodash");
const Projectinfo = require("../../../package.json");

class BootstrapConfig extends API {
  constructor(engine) {
    super(engine, "runtime");
  }

  register() {
    this.router.get('/', (req, res) => this.getConfig(this, req, res));
  }


  getConfig(self, req, res) {
    const config = {};

    self._version(config);
    self._api(config);
    self._runtime(config);


    res.set('Content-Type', 'application/javascript');
    res.render('bootstrap/bootstrap', _.extend(config, {
      layout: null
    }));
  }

  _version(config) {
    config.version = this.engine.version.version;
  }

  _runtime(config) {
    config.runtime = this.engine.app.runtime;

    const http = this.engine.config.http;
    if (http.port != 80 && http.port != 443) {
      config.path = `http://${this.engine.config.http.host}:${this.engine.config.http.port}/`;
    }else if(http.port == 80){
      config.path = `http://${this.engine.config.http.host}/`;
    }else if(http.port == 443){
      config.path = `https://${this.engine.config.http.host}/`;
    }

    config.server = `${config.path}${config.api}`;
    if (this.engine.app.runtime !== "local") {
      config.server = this.engine.config.runtime;
    }
  }

  _api(config) {
    config.name = this.engine.config.engine.name;
    config.api = "api/v1";
    config.cds = this.engine.config.cds;
    config.modules = JSON.stringify(Projectinfo.CDSEditorModules);
  }

}

module.exports = BootstrapConfig;
