"use strict";
const express = require('express');
const path = require('path');

module.exports = function Editor(engine) {
  engine.routes.build(__dirname);
  engine.views.registerView(__dirname);

  engine.app.use('/js/editor', express.static(path.join(__dirname, 'static', 'js')));
  engine.app.use('/style/editor', express.static(path.join(__dirname, 'static', 'style')));

  engine.app.get('/', (req, res) => {
    res.redirect('/editor');
  });

};
