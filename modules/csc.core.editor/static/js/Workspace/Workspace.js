define(function (require, exports, module) {
  "use strict";

  const View = require("Base/View");
  const WindowHookup = require("Utils/WindowHookup");
  const ActionHookup = require("Utils/ActionHookup");

  class Workspace extends View {
    constructor() {
      super("Views/workspace.html", true);
    }

    init() {
      WindowHookup.add("onReady", this._resizeWorkspace);
      WindowHookup.add("onResize", this._resizeWorkspace);

      ActionHookup.add("closeSidebar", this._resizeWorkspace);
      ActionHookup.add("clickSidebar", this._resizeWorkspace);
    }

    _resizeWorkspace() {
      let sidebarExt = 0;
      if ($("#sidebar-ext").hasClass("online"))
        sidebarExt = $("#sidebar-ext").width() + 1;

      $(".workspace").css({
        height: window.innerHeight - $(".dashboard #topbar").height() - 1,
      });

      $(".workspace .xcontainer").css({
        height: $(".workspace").height() - $(".workspace .tabs").height(),
      });

      $(".workspace").animate({
        width: window.innerWidth - $("#sidebar").width() - 1 - sidebarExt,
        left: $("#sidebar").width() + 1 + sidebarExt
      });
    }
  }


  module.exports = Workspace;
});
