define(function (require, exports, module) {
  "use strict";

  const Render = require("Base/Render");
  const Menu = require("Workspace/Menu");
  const Sidebar = require("Workspace/Sidebar");
  const Modal = require("Workspace/Modal");
  const Workspace = require("Workspace/Workspace");

  Render.pushView("Menu", new Menu());
  Render.pushView("Sidebar", new Sidebar());
  Render.pushView("Workspace", new Workspace());
  Render.pushView("Modal", new Modal());
});
