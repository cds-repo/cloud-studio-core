define(function (require, exports, module) {
  "use strict";

  const General = require("Utils/General");
  const WindowHookup = require("Utils/WindowHookup");
  const UIEvents = require("Utils/UIEvents");
  const Language = require("language");

  // TODO: refactor this class

  class OpenStack {
    constructor() {
      $(document).on("click", ".navi #prev", this.navBack);
      $(document).on("click", ".navi #next", this.navForward);

      const self = this;
      UIEvents.click(".tabs li .title", function titleClick(e) {
        self.clickElement.call(this, self, e);
      });
      UIEvents.click(".tabs li .close-tab", function titleClose(e) {
        self.closeElement.call(this, self, e);
      });

      WindowHookup.add("onReady", () => {
        this.restore();
      });

      window.onbeforeunload = function (e) {
        if ($(".workspace .tabs li.changed").length > 0)
          return Language.UNSAVED_NOTIFICATION;
      };

      this.restoreIdx = 0;
      this.activeElement = null;
    }

    navBack(e) {
      e.preventDefault();
      const et = $(this).parent().parent().children("ul");
      const left = parseInt(et.css('left'));

      if (left < 0)
        if (left + 85 > 0)
          et.animate({left: 0});
        else
          et.animate({left: left + 85});
    }

    navForward(e) {
      e.preventDefault();
      const et = $(this).parent().parent().children("ul");
      const left = parseInt(et.css('left'));

      var length = 0, last_length = 0;
      et.children('li').each((i, el)=> {
        length += $(el).width();
      });

      last_length = parseInt(et.children("li:last-child").width());

      if ($(".tabs").width() < length + 85)
        if (Math.abs(left) < Math.abs(length) - last_length - 85)
          et.animate({left: left - 85});
    }

    setMode(editor) {
      const session = editor.getSession();
      session.setFoldStyle("markbegin");
      editor.setSelectionStyle(false ? "line" : "text");
      editor.setHighlightActiveLine(true);
      editor.setDisplayIndentGuides(true);
      editor.setHighlightSelectedWord(true);
      editor.setAnimatedScroll(true);
      session.setUseSoftTabs(true);
      editor.setBehavioursEnabled(true);
      editor.setOption("scrollPastEnd", true);
      editor.setFontSize(13);

      editor.setOption({enableBasicAutocompletion: true, enableSnippets: true});
    }

    openTab(name, path, lang, type, content, noAdd) {
      $(".workspace .tabs li.active").removeClass('active');
      $(".workspace .xcontainer .active").removeClass('active');

      const tab = {
        unique_tab: "tab-" + (General.timestamp() - this.restoreIdx),
        name: name,
        file: path + name,
        path: path
      };

      if (!noAdd)
        this.add(tab.file, "file");
      else
        this.restoreIdx = this.restoreIdx - 1;

      $(".workspace .tabs ul").append(Mustache.render($("#tab-tpl").text(), tab));


      if (type === "image") {
        $(".workspace .xcontainer").append(Mustache.render($("#img-ctnt").text(), tab));
        this._loadImage(tab, content);
      } else {
        $(".workspace .xcontainer").append(Mustache.render($("#tab-ctnt").text(), tab));
        this._loadEditor(tab, content);
      }

      $(".workspaceLoader").hide();
    }

    _loadImage(tab, content) {
      const img = $("<img/>", {
        alt: "image",
        class: "img-responsive",
        src: "data:image/png;base64," + content.trim()
      });

      $(`#${tab.unique_tab} .image-box`).append(img);
    }

    _loadEditor(tab, content) {
      $(`#${tab.unique_tab} .textarea`).text(content);

      const editor = ace.edit(tab.unique_tab + "-textarea");
      editor.setTheme("ace/theme/eclipse");
      editor.getSession().setMode('ace/mode/' + lang);

      editor.getSession().on("change", (el) => {
        const e = `.workspace .tabs li[data-container="${tab.unique_tab}"]`;
        if (!$(e).hasClass('changed')) {
          $(e).data('changed', true);
          $(e).addClass("changed");
        }
      });
      this.setMode(editor);
      $(`#${tab.unique_tab}`).data('editor', editor);
    }

    restore() {
      let all = localStorage.getItem("Project-" + CDSConfig.Project);

      if (all === null || all === undefined)
        all = "{ }";

      all = JSON.parse(all);

      _.each(all, (v, k)=> {
        $.ajax({
          async: true,
          dataType: 'json',
          type: 'GET',
          url: APIPath(`${CDSConfig.Project}/read`),
          data: {
            path: k
          },
          success: (data) => {
            this.openTab(data.name, data.path, data.lang, data.type, data.content, true);
          },
          error: (e) => {
            delete all[k];
            localStorage.setItem("Project-" + CDSConfig.Project, JSON.stringify(all));
          }
        });
      });
    }

    empty() {
      return $(".workspace .tabs li").length === 0;
    }

    clickElement(self, element) {
      $(".workspace .tabs li.active").removeClass('active');
      $(".workspace .xcontainer section.active").removeClass('active');

      const box = $(this).parent().data('container');
      $(this).parent().addClass('active');
      const elemId = `.workspace .xcontainer section#${box}`;
      $(elemId).addClass('active');

      if (!$(elemId).hasClass("wbuilder")) {
        if ($("#sidebar-ext").css('display') !== "none" && $("#sidebar-ext .active").attr("id") === "TOOLBOX") {
          $("#TOOLBOX .close-widget").click();
        }
      } else {
        if ($("#sidebar-ext").css('display') !== "none" && $("#sidebar-ext .active").attr("id") !== "TOOLBOX") {
          $("a[data-widget=TOOLBOX]").click();
        }
      }

      $(".workspaceLoader").hide();

      self.activeElement = this;
    }

    lastActive() {
      if (this.empty()) {
        return;
      }

      if (this.activeElement === null) {
        this.activeElement = $(".tabs li").last().children(".title");
      }

      this.clickElement(this, this.activeElement);
    }

    closeElement(self, element) {
      if ($(this).parent().hasClass('changed')) {
        const iConfirm = confirm("Are you sure you want to close the file?");

        if (!iConfirm) {
          return;
        }
      }

      const hasActive = $(this).parent().hasClass("active");
      const box = $(this).parent().data('container');

      $(this).parent().remove();
      $(`.workspace .xcontainer section#${box}`).remove();

      self.remove($(this).parent().data('file'));

      if (hasActive) {
        self.activeElement = null;
        self.lastActive();
      }
    }

    add(element, info) {
      let all = localStorage.getItem(`Project-${CDSConfig.Project}`);
      if (all === null || all === undefined) {
        all = "{ }";
      }
      all = JSON.parse(all);

      all[element] = info;
      localStorage.setItem(`Project-${CDSConfig.Project}`, JSON.stringify(all));
    }

    remove(element) {
      let all = localStorage.getItem(`Project-${CDSConfig.Project}`);
      if (all === null || all === undefined) {
        all = "{ }";
      }
      all = JSON.parse(all);

      _.each(all, (v, k) => {
        if (k === element) {
          delete all[k];
        }
      });

      localStorage.setItem(`Project-${CDSConfig.Project}`, JSON.stringify(all));
    }
  }

  const openStack = new OpenStack();
  module.exports = openStack;
});
