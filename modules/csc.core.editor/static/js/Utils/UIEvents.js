define(function (require, exports, module) {
  "use strict";

  class UIEvents {
    constructor() {
    }

    click(element, callback) {
      $(document).on("click", element, function clickOnElement(e) {
        e.preventDefault();

        if (callback !== undefined) {
          callback.call(this, e);
        } else {
          console.log(`CLICK @ ${element}`);
          return false;
        }
      });
    }

    remove(event, element) {
      $(document).off(event, element);
    }

    trigger(event, element) {
      // console.log(`Triggering: [${event}] on: [${element}]`);
      $(element).trigger(event);
    }
  }

  const uiEvents = new UIEvents();

  module.exports = uiEvents;
});
