define(function (require, exports, module) {
  "use strict";

  module.exports = {
    timestamp: () => Math.round(new Date().getTime() / 1000),

    error_handling: (jqXHR, textStatus, errorThrown) => {
      if (jqXHR.responseJSON == null) {
        console.log(jqXHR.responseJSON);
      } else {
        var e = JSON.parse(jqXHR.responseText);
        console.log(e);
      }
    },

    unique_id_name: (name) => {
      const e = name.replace(" ", "-");
      let q = e;
      let i = 2;

      while ($("#" + q).length != 0) {
        q = e + "-" + i;
        i++;
      }

      return q;
    }
  };
});
