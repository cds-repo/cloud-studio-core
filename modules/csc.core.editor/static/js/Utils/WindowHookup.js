define(function (require, exports, module) {
  "use strict";

  const Hookup = require("Utils/Hookup");
  const WindowHookup = new Hookup({
    onReady: [],
    onResize: []
  });
  
  module.exports = WindowHookup;
});
