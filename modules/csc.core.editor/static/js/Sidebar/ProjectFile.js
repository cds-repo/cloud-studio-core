define(function (require, exports, module) {
  "use strict";

  const Sidebar = require("Base/Sidebar");
  const General = require("Utils/General");
  const OpenStack = require("Workspace/OpenStack");
  const Language = require("language");
  const Message = require("Base/Message");

  class ProjectFile extends Sidebar {
    constructor() {
      super("PROJECT-FILES");
      this._contextMenu(`#${this.id} .content`);
    }

    fill(callback) {
      $.ajax({
        url: APIPath(`${CDSConfig.Project}/tree?path=/`),
        data: {},
        success: (data) => {
          const element = `#${this.id} .content`;
          $(element).text("");
          $(element).append(this.parsePaths(0, data, ''));
          this._loadActions(element);

          if (callback !== undefined)
            callback();
        },
        dataType: 'json',
        type: 'GET',
        error: General.error_handling
      });
    }

    parsePaths(id, data, base) {
      let path = `<ul id="folder-${id}">`;
      _.each(data, (v) => {
        path += '<li> <a';

        if (v.type == "directory") {
          path += ` data-folder="${v.id}"`;
        } else {
          path += ` id="file-${v.id}"`;
        }

        if (v.file.search(".app") >= 0) {
          path += ' data-type="app"';
        }

        path += ` data-path="${base}" data-name="${v.file}" class="${v.type}">`;
        path += `<span class="glyphicon ${this.iconType(v.type)}"></span> <span class="elementname">${v.file}</span></a>`;


        if (v.type == "directory") {
          path += `\n${this.parsePaths(v.id, v.content, `${base}${v.file}/`)}`;
        }

        path += "</li>\n";
      });

      return path + "</ul>\n";
    }

    iconType(type) {
      switch (type) {
        case "app":
          return "glyphicon-list-alt";
          break;
        case "directory":
          return "glyphicon-folder-close";
          break;
        case "image":
          return "glyphicon-picture";
          break;
        default:
          return "glyphicon-file";
      }
    }

    _loadActions(element) {
      const that = this;
      $(document).off("click", `${element} .directory`);
      $(document).off("click", `${element} .file`);
      // $(document).off("click", `${element} .app`);
      $(document).off("click", `${element} .image`);


      $(document).on("click", `${element} .directory`, this._folderOpen);
      $(document).on("click", `${element} .file`, function (e) {
        that._fileOpen(this, element)
      });
      $(document).on("click", `${element} .image`, function (e) {
        that._fileOpen(this, element)
      });
    }

    _folderOpen(e) {
      e.preventDefault();

      $(this).children(".glyphicon").toggleClass("glyphicon-folder-open");
      $(this).children(".glyphicon").toggleClass("glyphicon-folder-close");
      $(this).parent().children("#folder-" + $(this).data('folder')).slideToggle();
    }

    _fileOpen(that, element) {
      $(".workspaceLoader").show();
      const elem = $(that);

      const tab_elem = $(`.workspace .tabs li[data-file="${elem.data('path')}${elem.data('name')}"]`);
      if (tab_elem.length > 0) {
        OpenStack.clickElement(OpenStack, tab_elem.children('.title'));
        return;
      }

      $(`${element} .file.active-file`).removeClass('active-file');
      elem.addClass('active-file');

      const the_file = `${elem.data('path')}${elem.data('name')}`;

      $.ajax({
        url: APIPath(`${CDSConfig.Project}/read`),
        data: {
          path: the_file
        },
        success: function (data) {
          OpenStack.openTab(elem.data('name'), elem.data('path'), data.lang, data.type, data.content);
        },
        dataType: 'json',
        type: 'GET',
        error: General.error_handling
      });
    }

    _contextMenu(element) {
      const self = this;
      const options = {
        callback: function (key, options) {
          if ($(this.context).hasClass('content')) {
            self[key](null);
          } else {
            self[key]($(this.context).children());
          }
        },
        events: {
          show: function showCtxMenu() {
            $(this.context).children().addClass('focused');
          },
          hide: function hideCtxMenu() {
            $(this.context).children().removeClass('focused');
          }
        },
        items: {
          newFile: {name: Language.CTX_NEWFILE, icon: "add"},
          newFolder: {name: Language.CTX_NEWFOLDER, icon: "folder"},
          sep1: "---------",
          rename: {name: Language.CTX_RENAME, icon: "edit"},
          delete: {name: Language.CTX_DELETE, icon: "delete"},
          sep2: "---------",
          refill: {name: Language.CTX_REFRESH, icon: "refresh"}
        }
      };

      const optionsRandomLocation = _.cloneDeep(options);
      delete optionsRandomLocation.items.sep2;
      delete optionsRandomLocation.items.rename;
      delete optionsRandomLocation.items.delete;

      $.contextMenu(_.extend(optionsRandomLocation, {
        selector: `${element}`
      }));
      $.contextMenu(_.extend(options, {
        selector: `${element} li`
      }));

    }

    newFolder(element) {
      Message.Prompt(Language.NEWFOLDER_LABEL,
        `folder-${General.timestamp()}`,
        (name) => {
          let path = "/";
          if (element !== null) {
            path = element.data('path');

            if (element.hasClass('directory'))
              path += element.data('name');

            path += "/";
          }

          if (name === undefined || name === null || name.length === 0) {
            return false;
          }

          $(".contentLoader").show();
          $.ajax({
            url: APIPath(`${CDSConfig.Project}/create`),
            data: {
              path: `${path}${name}`,
              type: "folder"
            },
            success: (data) => {
              if (data.status === "ok") {
                this.refill();
              } else
                Message.Error(data.message).show();

              $(".contentLoader").hide();
            },
            dataType: 'json',
            type: 'POST',
            error: (e) => {
              $(".contentLoader").hide();
              this.refill();
              Message.Error(e.responseJSON.message).show();
            }
          });

        })
        .show();
    }

    newFile(element) {
      Message.Prompt(Language.NEWFILE_LABEL,
        `untitled-${General.timestamp()}.txt`,
        (name) => {
          let path = "/";
          if (element !== null) {
            path = element.data('path');

            if (element.hasClass('directory'))
              path += element.data('name');

            path += "/";
          }

          if (name === undefined || name === null || name.length === 0) {
            return false;
          }

          $(".contentLoader").show();
          $.ajax({
            url: APIPath(`${CDSConfig.Project}/create`),
            data: {
              path: `${path}${name}`,
              type: "file"
            },
            success: (data) => {
              if (data.status === "ok") {
                this.refill();
                OpenStack.openTab(data.name, data.path, data.lang, data.type, data.content);
              } else
                Message.Error(data.message).show();

              $(".contentLoader").hide();
            },
            dataType: 'json',
            type: 'POST',
            error: (e) => {
              $(".contentLoader").hide();
              this.refill();
              Message.Error(e.responseJSON.message).show();
            }
          });

        })
        .show();
    }

    rename(element) {
      Message.Prompt(Language.RENAME_LABEL,
        element.data('name'),
        (name) => {
          $(".contentLoader").show();

          $.ajax({
            url: APIPath(`${CDSConfig.Project}/move`),
            data: {
              source: `${element.data('path')}${element.data('name')}`,
              destination: `${element.data('path')}${name}`
            },
            success: (data) => {
              if (data.status === "ok") {

                let $e = $(`.workspace .tabs li[data-file="${(element.data('path') + element.data('name')).replace("//", "/")}"]`);
                if ($e.length > 0) {
                  $e.children('.title').html(name);
                  $e.data('name', name);
                  $e.data('file', $element.data('path') + name);
                }
                this.refill();
              } else
                Message.Error(data.message).show();

              $(".contentLoader").hide();
            },
            dataType: 'json',
            type: 'PUT',
            error: (e) => {
              $(".contentLoader").hide();
              this.refill();
              Message.Error(e.responseJSON.message).show();
            }
          });

        })
        .show();
    }

    delete(element) {
      Message.Confirm(Language.DELETE_CONFIRMATION.replace("%s", element.data('name')),
        () => {
          $(".contentLoader").show();

          $.ajax({
            url: APIPath(`${CDSConfig.Project}/delete`),
            data: {
              path: `${element.data('path')}${element.data('name')}`,
              type: element.hasClass('directory') ? "folder" : "file"
            },
            success: (data) => {
              if (data.status === "ok") {

                let $e = $(`.workspace .tabs li[data-file="${(element.data('path') + element.data('name')).replace("//", "/")}"]`);
                if ($e.length > 0) {
                  OpenStack.closeElement(OpenStack, $e.children('.title'));
                }
                this.refill();
              } else
                Message.Error(data.message).show();

              $(".contentLoader").hide();
            },
            dataType: 'json',
            type: 'DELETE',
            error: (e) => {
              let $e = $(`.workspace .tabs li[data-file="${(element.data('path') + element.data('name')).replace("//", "/")}"]`);
              if ($e.length > 0) {
                OpenStack.closeElement(OpenStack, $e.children('.title'));
              }
              $(".contentLoader").hide();
              this.refill();
              Message.Error(e.responseJSON.message).show();
            }
          });

        })
        .show();
    }
  }

  module.exports = ProjectFile;
});
