define(function (require, exports, module) {
  "use strict";

  const ProjectFile = require("Sidebar/ProjectFile");

  class SidebarWrapper {
    constructor() {
      this._sidebars = [];
    }

    add(name, sidebar) {
      this._sidebars.push({name, sidebar});
    }

    addAfter(index, name, sidebar) {
      if (_.isNumber(index)) {
        this._sidebars.splice(index, 0, {name, sidebar});
        return;
      }

      let ix = -1;

      _.each(this._sidebars, (v, k) => {
        if (v.name === index) {
          ix = k;
          return;
        }
      });

      if (ix < 0) {
        this._sidebars.push({name, sidebar})
      } else {
        this._sidebars.splice(ix + 1, 0, {name, sidebar});
      }
    }

    get sidebar() {
      return this._sidebars;
    }

    get(name) {
      let toReturn = _.filter(this._sidebars, (sidebar) => sidebar.name === name);

      if (toReturn.length === 0) {
        return null;
      }
      toReturn = toReturn.pop().sidebar;

      return toReturn;
    }
  }

  const sidebarWrapper = new SidebarWrapper();
  sidebarWrapper.add('ProjectFile', new ProjectFile());

  module.exports = sidebarWrapper;
});
