define(function (require, exports, module) {
  "use strict";

  module.exports = (CDS) => {
    const SampleModule = require("./sample/index");

    new SampleModule(CDS);
  }
});
