define(function (require, exports, module) {
  "use strict";

  const Menu = require("Base/Menu");
  const UIEvents = require("Utils/UIEvents");
  const Message = require("Base/Message");
  const Modal = require("Base/Modal");
  const Language = require("language");

  class SampleMenu extends Menu.Menu {
    constructor() {
      super("Modules/sample/SampleMenu");
    }

    initEvents() {
      UIEvents.click("#SAMPLE_BUTTON", this._about);
    }

    _about() {
      Message
        .Notification(Language.SAMPLE_MESSAGE, "About")
        .prepare(Modal.OK_CANCEL)
        .show();
    }
  }

  module.exports = SampleMenu;
});
