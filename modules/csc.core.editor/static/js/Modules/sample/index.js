define(function (require, exports, module) {
  "use strict";

  const Module = require("Base/Module");
  const MenuWrapper = require("Menu/index");

  const SampleMenu = require("./SampleMenu/index");

  class SampleModule extends Module {
    init() {
      MenuWrapper.addAfter("Project", "Sample", new SampleMenu());
    }
  }

  module.exports = SampleModule;
});
