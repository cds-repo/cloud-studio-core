/*global require, define, $, window, navigator, Mustache */

requirejs.config({
  paths: {
    "text": "/bower_components/text/text",
    "i18n": "/bower_components/i18n/i18n"
  },

  locale: window.localStorage.getItem("locale") || (typeof (CDSConfig) !== "undefined" ? CDSConfig.language : navigator.language)
});

define(function (require, exports, module) {
  "use strict";

  const CDSObject = require("Base/CDSObject");
  const CDS = new CDSObject();
  window.CDS = CDS;
  CDS.Hookup = require("Utils/Hookup");

  CDS.ActionHookup = require("Utils/ActionHookup");
  CDS.WindowHookup = require("Utils/WindowHookup");

  CDS.Language = require("language");

  CDS.Modal = require("Base/Modal");
  CDS.Render = require("Base/Render");
  CDS.View = require("Base/View");
  CDS.Message = require("Base/Message");
  CDS.Menu = require("Base/Menu");
  CDS.Sidebar = require("Base/Sidebar");

  CDS.General = require("Utils/General");
  CDS.Shortcut = require("Utils/Shortcut");
  CDS.UIEvents = require("Utils/UIEvents");

  CDS.Workspace = require("Workspace/index");
  CDS.OpenStack = require("Workspace/OpenStack");

  require("Modules/index")(CDS);

  const lazyRender = _.debounce(() => {
    CDS.Render.render();
  }, 100);

  const lazyResize = _.debounce(() => {
    CDS.WindowHookup.execute("onResize");
  }, 400);

  const lazyReady = _.debounce(() => {
    CDS.WindowHookup.execute("onReady");
    CDS.UIEvents.trigger("click", "#FILE_OPEN");
    $(".loader").fadeOut();
  }, 400);

  lazyRender();
  $(window).resize(lazyResize);
  $(document).ready(lazyReady);

  module.exports = CDS;
});
