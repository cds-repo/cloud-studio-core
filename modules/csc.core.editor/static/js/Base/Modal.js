define(function (require, exports, module) {
  "use strict";

  const Language = require("language");

  class Modal {
    constructor(modalID, type) {
      this.clear();
      this.afterShow = null;
      this.beforeShow = null;
      this.id = modalID;
      this.type = type;

      this.initView();
      this.initEvents();
    }

    static get YES_NO() {
      return 1;
    }

    static get OK_CANCEL() {
      return 2;
    }

    static get SAVE_CANCEL() {
      return 3;
    }

    static get CREATE_CANCEL() {
      return 4;
    }

    initView() {
      this.yesButton = $(`${this.id} .modal-yes`);
      this.noButton = $(`${this.id} .modal-no`);
      this.titleTxt = $(`${this.id} .modal-title`);
      this.bodyTxt = $(`${this.id} .modal-body`);
      this.modalElement = $(`${this.id}`);
    }

    initEvents() {
      const self = this;

      $(document).off("click", `${self.id} .modal-yes`);
      $(document).on("click", `${self.id} .modal-yes`, function (e) {
        const action = $(this).data("action");

        if (action != undefined && action != null && action != "") {
          action();
        }

        self.modalElement.modal("hide");
      });
    }

    clear() {
      this.data = {};
      this.title = "";
      this.text = "";
      this.action = null;

      return this;
    }

    pushData(element, value) {
      this.data[element] = value;

      return this;
    }

    prepare(type) {
      if (_.isNumber(type)) {
        this.type = type;
      }

      switch (this.type) {
        default:
        case Modal.YES_NO:
          this.yesButton.text(Language.YES);
          this.noButton.text(Language.NO);
          break;
        case Modal.OK_CANCEL:
          this.yesButton.text(Language.OK);
          this.noButton.text(Language.CANCEL);
          break;
        case Modal.SAVE_CANCEL:
          this.yesButton.text(Language.SAVE);
          this.noButton.text(Language.CANCEL);
          break;
        case Modal.CREATE_CANCEL:
          this.yesButton.text(Language.CREATE);
          this.noButton.text(Language.CANCEL);
          break;
      }

      return this;
    }

    make() {
      this.titleTxt.html(this.title);
      this.bodyTxt.html(this.text);
      this.yesButton.data("action", this.action);
      this.prepare();

      _.each(this.data, (v, k)=> {
        this.yesButton.data(k, v);
      });

      return this;
    }

    show() {
      if (this.beforeShow) {
        this.beforeShow();
      }
      this.modalElement.modal("show");
      if (this.afterShow) {
        this.afterShow();
      }
    }
  }

  module.exports = Modal;
});
