define(function (require, exports, module) {
  "use strict";

  class MenuItem {
    constructor(settings) {
      this._options = _.extend({
        name: "",
        icon: "fa-none",
        shortcut: "",
        type: "",
        text: "",
        disable: false,
        submenu: []
      }, settings);
    }

    get(element) {
      return this._options[element];
    }

    forge(text) {
      this._options.text = text;
      return this._options;
    }
  }

  class MenuButton extends MenuItem {
    constructor(settings) {
      settings.type = "button";
      super(settings);
    }
  }

  class MenuDivider extends MenuItem {
    constructor(settings) {
      settings.type = "divider";
      super(settings);
    }
  }

  class MenuDropDown extends MenuItem {
    constructor(settings, submenu) {
      super(settings);
      this.initSubMenu(submenu);
    }

    initSubMenu(submenu) {
      let submenuList = [];

      for (let i = 0; i < submenu.length; i++) {
        let item;
        switch (submenu[i].type) {
          case "button":
            item = new MenuButton(submenu[i]);
            break;
          case "divider":
            item = new MenuDivider(submenu[i]);
            break;
        }
        submenuList.push(item);
      }

      this._options.type = "dropdown";
      this._options.submenu = submenuList;
    }
  }

  class Menu {
    constructor(path) {
      const self = this;

      require([`text!${path}/definition.json`], (definition) => {
        self.definition = JSON.parse(definition);

        self.parse();
      });

      self.initEvents();
    }

    parse() {
      if (!_.isArray(this.definition.submenu) || this.definition.submenu.length === 0) {
        this.definition = new MenuButton(this.definition.description);
      } else {
        this.definition = new MenuDropDown(this.definition.description, this.definition.submenu);
      }
    }

    initEvents() {

    }

    render() {

    }
  }

  // function MenuDisable(element) {
  //   let _element = element.replace(".", "_").toUpperCase();
  //   $("#" + _element).addClass("disabled");
  // }
  //
  // function MenuEnable(element) {
  //   let _element = element.replace(".", "_").toUpperCase();
  //   $("#" + _element).removeClass("disabled");
  // }

  module.exports = {
    Menu,
    MenuItem,
    MenuButton,
    MenuDropDown,
    MenuDivider
  };
});
