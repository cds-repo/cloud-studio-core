define(function (require, exports, module) {
  "use strict";

  const Menu = require("Base/Menu");
  const UIEvents = require("Utils/UIEvents");
  const Sidebar = require("Sidebar/index");
  const ProjectFiles = Sidebar.get("ProjectFile");
  const Message = require("Base/Message");
  const Language = require("language");
  const OpenStack = require("Workspace/OpenStack");

  class File extends Menu.Menu {
    constructor() {
      super("Menu/File");
    }

    initEvents() {
      const self = this;
      UIEvents.click("#FILE_NEW", this._newFile);
      UIEvents.click("#FILE_OPEN", this._openFile);
      UIEvents.click("#FILE_SAVE", function (e) {
        self._saveFile.call(this, self, e);
      });
      UIEvents.click("#FILE_SAVE_ALL", function (e) {
        self._saveAllFiles.call(this, self, e);
      });
      UIEvents.click("#FILE_CLOSE", this._closeTab);
      UIEvents.click("#FILE_EXIT", this._closeWindow);
    }

    _newFile() {
      ProjectFiles.newFile();
    }

    _openFile() {
      UIEvents.trigger("click", "a[data-widget=\"PROJECT-FILES\"]");
    }

    _getContent(tab_id) {
      const result = {
        data: '',
        type: ''
      };

      const $container = $(`.workspace .content#${tab_id}`);

      if ($container.data('editor') != null) {
        result.data = $container.data('editor').getValue();
        result.type = "page";
      } else if ($container.data('type') == "app") {
        result.data = JSON.stringify({
          style: $container.children('style').text(),
          layout: $container.children('.layout').html(),
          actions: $container.children('.actions').text()
        });
        result.type = "app";
      }

      return result;
    }

    _saveFile(self) {
      return self._save($(".workspace .tabs li.active"));
    }

    _saveAllFiles(self) {
      $(".workspace .tabs li.changed").each(function (i, e) {
        self._save($(this));
      });
    }

    _save(activeTab) {
      $(`.workspace .content#${activeTab
        .data('container')}`).children(".fader").show();

      activeTab.removeClass("changed");
      activeTab.children(".scroll").css({display: "inline-block"});

      const content = this._getContent(activeTab.data('container'));
      const file = activeTab.data('file');

      $.ajax({
        url: APIPath(`${CDSConfig.Project}/save`),
        data: {
          path: file,
          content: content.data,
          type: content.type
        },
        success: function (data) {
          activeTab.children(".scroll").hide();
          $(`.workspace .content#${activeTab.data('container')}`).children(".fader").hide();
        },
        dataType: 'json',
        type: 'PUT',
        error: function (e) {
          activeTab.addClass("changed");
          Message.Error(e.responseJSON.message);
        }
      });
    }

    _closeTab() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE);
        return;
      }

      const active_tab = $(".workspace .tabs li.active");
      OpenStack.closeElement(active_tab.children(".title"));
    }

    _closeWindow() {
      $(location).attr('href', 'about:blank');
    }
  }

  module.exports = File;
});
