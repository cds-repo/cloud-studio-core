define(function (require, exports, module) {
  "use strict";

  const Menu = require("Base/Menu");
  const UIEvents = require("Utils/UIEvents");
  const OpenStack = require("Workspace/OpenStack");
  const Message = require("Base/Message");
  const Language = require("language");

  class Edit extends Menu.Menu {
    constructor() {
      super("Menu/Edit");
    }

    initEvents() {
      UIEvents.click("#EDIT_UNDO", this._undo);
      UIEvents.click("#EDIT_REDO", this._redo);
      UIEvents.click("#EDIT_CUT", this._browserRestricted);
      UIEvents.click("#EDIT_COPY", this._browserRestricted);
      UIEvents.click("#EDIT_PASTE", this._browserRestricted);
      UIEvents.click("#EDIT_FIND", this._find);
      UIEvents.click("#EDIT_FIND_NEXT", this._findNext);
      UIEvents.click("#EDIT_FIND_PREV", this._findPrev);
      UIEvents.click("#EDIT_REPLACE", this._replace);
      UIEvents.click("#EDIT_GOTO", this._goto);
    }

    _undo() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE).show();
        return;
      }

      const active_tab = $(".workspace .tabs li.active");
      $(`.workspace .content#${active_tab.data('container')}`).data('editor').undo();
    }

    _redo() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE).show();
        return;
      }

      const active_tab = $(".workspace .tabs li.active");
      $(`.workspace .content#${active_tab.data('container')}`).data('editor').redo();
    }

    _browserRestricted() {
      Message.Notification(Language.BROWSER_RESTRICTED).show();
    }

    _goto() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE).show();
        return;
      }

      const line = prompt("Jump to line:", "");
      if (line && !isNaN(Number(line))) {
        const active_tab = $(".workspace .tabs li.active");
        $(`.workspace .content#${active_tab.data('container')}`).data('editor').gotoLine(Number(line));
      }
    }

    _find() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE).show();
        return;
      }

      const active_tab = $(".workspace .tabs li.active");
      $(`.workspace .content#${active_tab.data('container')}`).data('editor').execCommand('find');
    }

    _findNext() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE).show();
        return;
      }

      const active_tab = $(".workspace .tabs li.active");
      $(`.workspace .content#${active_tab.data('container')}`).data('editor').execCommand('findNext');
    }

    _findPrev() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE).show();
        return;
      }

      const active_tab = $(".workspace .tabs li.active");
      $(`.workspace .content#${active_tab.data('container')}`).data('editor').execCommand('findPrev');
    }

    _replace() {
      if (OpenStack.empty()) {
        Message.Error(Language.OPEN_A_FILE).show();
        return;
      }

      const active_tab = $(".workspace .tabs li.active");
      $(`.workspace .content#${active_tab.data('container')}`).data('editor').execCommand('replace');
    }
  }

  module.exports = Edit;
});
