"use strict";
const Route = require("../../../engine/core/Route");

class Editor extends Route {
  constructor(engine) {
    super(engine, 'editor');
  }

  register() {
    this.router.get('/', (req, res) => this.getEditor(this, req, res));
  }

  getEditor(self, req, res) {
    if (self.engine.app.runtime !== "local") {
      self.redirectUnauthenticated(req.user, res)
    }

    res.render('editor', {
      user: req.user,
      layout: null
    });
  }
}

module.exports = Editor;
