"use strict";

const args = process.argv;

if (args.length < 3) {
  console.log("Usage: node bin/run.toolkit [toolkit_file [toolkit_file [...]]]");
  process.exit(1);
}
args.shift();
args.shift();

for (let i = 0; i < args.length; i++) {
  let script = require(`../toolkit/${args[i]}`)();
  script.run();
  script = null;
}
